
pyborgun
==============

pyborgun 0.1.0


Introduction
===========
Pyborgun is a Python package that provides interfaces to Borgun Payment Gateway.

The goal of pyborgun is to support the full breadth and depth of Borgun Payment Gateway.


Installation
==============

Install from source:

	$ git clone https://bitbucket.org/icewise/pyborgun.git
	$ cd pybokun
	$ python setup.py install

Gettins Started with Pyborgun

Your credentials can be passed into the methods that create connections. Alternatively, pyborgun will check for the existance of the following environment variables to ascertain your credentials:

*BORGUN_USERNAME* - Your Borgun username
*BORGUN_PASSWORD* - Your Borgun password