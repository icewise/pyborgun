#-*- coding: utf-8 -*-


class BorgunError(Exception):

    """Base class for all errors."""


class BorgunConnectionError(BorgunError):

    """Error communicating with the Borgun.is API."""


class BorgunResponseError(BorgunError):

    """Error response code returned from API."""


class BorgunInvalidError(BorgunError):

    """Invalid information provided."""
