#-*- coding: utf-8 -*-
"""
This module provides the data structures for describing credit cards and
addresses for use in executing charges.
"""

import calendar
from datetime import datetime
import re
from borgun.exceptions import BorgunInvalidError


class CreditCard(object):

    """
    Represents a credit card that can be charged.

    Pass in the credit card number, expiration date, CVV code, and optionally
    a first name and last name. The card will be validated upon instatiation
    and will raise an
    :class:`BorgunInvalidError <borgun.exceptions.BorgunInvalidError>`
    for invalid credit card numbers, past expiration dates, etc.
    """

    def __init__(self, card_number=None, exp_year=None, exp_month=None,
                 cvv=None, name=None):
        """
        :param str card_number: The card number
        :param str exp_year: The exp yeaer
        :param str exp_month: The exp month
        :param str cvv: The cvv
        :param str name: The name
        """
        self.card_number = re.sub(r'\D', '', str(card_number))
        self._exp_year = str(exp_year)
        self._exp_month = str(exp_month)
        self.cvv = str(cvv)
        self.validate()

    def __repr__(self):
        return '<CreditCard {0.card_type} {0.safe_number}>'.format(self)

    def validate(self):
        """
        Validates the credit card data and raises an
        :class:`BorgunInvalidError <borgun.exceptions.BorgunInvalidError>`
        if anything doesn't check out. You shouldn't have to call this
        yourself.
        """
        try:
            num = map(int, self.card_number)
        except ValueError:
            raise BorgunInvalidError('Credit card number is not valid.')
        if sum(num[::-2] + map(lambda d: sum(divmod(d * 2, 10)), num[-2::-2])) % 10:
            raise BorgunInvalidError('Credit card number is not valid.')
        if datetime.now() > self.expiration:
            raise BorgunInvalidError('Credit card is expired.')
        if not re.match(r'^[\d+]{3,4}$', self.cvv):
            raise BorgunInvalidError('Credit card CVV is invalid format.')

    @property
    def expiration(self):
        """
        The credit card expiration date as a ``datetime`` object.

        :rtype: datetime.date
        """
        return datetime(int(self._exp_year), int(self._exp_month),
                        calendar.monthrange(
                            int(self._exp_year), int(self._exp_month))[1],
                        23, 59, 59)

    @property
    def exp_year(self):
        """
        :rtype: str
        """
        return self.expiration.strftime("%y")

    @property
    def exp_month(self):
        """
        :rtype: str
        """
        return self.expiration.strftime("%m")

    @property
    def safe_number(self):
        """
        The credit card number with all but the last four digits masked. This
        is useful for storing a representation of the card without keeping
        sensitive data.

        :rtype: str
        """
        mask = '*' * (len(self.card_number) - 4)
        return '{0}{1}'.format(mask, self.card_number[-4:])


class BorgunResponse(object):

    def __init__(self, action_code, raw_data, **options):
        """
        :param str action_code: The action code
        :param str raw_data: The raw data
        """
        self.action_code = action_code
        self.data = raw_data
        self.authorization = options.pop('authorization', None)
        for k, v in options.items():
            setattr(self, k, v)

    @property
    def success(self):
        """
        :rtype: bool
        """
        return self.action_code == "000"

    @property
    def message(self):
        """
        :rtype: str
        """
        return ActionCodes[self.action_code]

    def __unicode__(self):
        if self.action_code in ActionCodes:
            return ActionCodes[self.action_code]
        return

    def __repr__(self):
        return u'<borgun.%s: %s>' % (self.__class__.__name__, unicode(self))


ActionCodes = {
    "000": "Accepted",
    "100": "Do not honor (Not Accepted)",
    "101": "Expired card",
    "102": "Suspected card forgery (fraud)",
    "103": "Merchant call acquirer",
    "104": "Restricted card",
    "106": "Allowed PIN retries exceeded",
    "109": "Merchant not identified",
    "110": "Invalid amount",
    "111": "Invalid card number",
    "112": "Pin required",
    "116": "Not sufficient funds",
    "117": "Invalid PIN",
    "118": "Unknown card",
    "119": "Transaction not allowed to cardholder",
    "120": "Transaction not allowed to terminal",
    "121": "Exceeds limits to withdrawal",
    "125": "Card not valid",
    "126": "False PIN block",
    "129": "Suspected fraud",
    "130": "Invalid Track2",
    "131": "Invalid expiration date",
    "161": "DCC transaction allowed to cardholder",
    "162": "DCC cardholder currency not supported",
    "163": "DCC exceeds time limit for withdrawal",
    "164": "DCC transaction not allowed to terminal",
    "165": "DCC not allowed to merchant",
    "166": "DCC unknown error",

    "200": "Do not honor",
    "201": "Card not valid",
    "202": "Suspected card forgery (fraud)",
    "203": "Merchant contact acquirer",
    "204": "Limited card",
    "205": "Merchant contact police",
    "206": "Allowed PIN-retries exceeded",
    "207": "Special occasion",
    "208": "Lost card",
    "209": "Stolen card",
    "210": "Suspected fraud",

    "902": "False transaction",
    "904": "Form error",
    "907": "Issuer not responding",
    "908": "Message can not be routed",
    "909": "System error",
    "910": "Issuer did not respond",
    "911": "Issuer timed out",
    "912": "Issuer not reachable",
    "913": "Double transaction transmission",
    "914": "Original transaction can not be traced",
    "916": "Merchant not found",
    "950": "No financial records found for detail data",
    "951": "Batch already closed",
    "952": "Invalid batch number",
    "953": "Host timeout",
    "954": "Batch not closed",
    "955": "Merchant not active",
    "956": "Transaction number not unique"
}
