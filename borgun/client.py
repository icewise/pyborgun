#-*- coding: utf-8 -*-
'''
This module contains the client API to communicate with Borgun.
'''
import os
from suds.client import Client as SudsClient
from suds.client import SoapClient
from datetime import datetime
from borgun.data import CreditCard, BorgunResponse
import xml.etree.ElementTree as ET



_ROOT = os.path.dirname(os.path.abspath(__file__))

TEST_URL = 'file:///'+os.path.join(_ROOT, 'TestHeimir.pub.ws-Authorization.xml')
PROD_URL = 'file:///'+os.path.join(_ROOT, 'Heimir.pub.ws-Authorization')

import logging
log = logging.getLogger(__name__)


class TestClient(SoapClient):

    def failed(self, binding, error):
        raise error

SoapClient = TestClient


def mask_cc(cc):
    return "*" * (len(cc) - 4) + cc[-4:]


class BorgunClient(object):

    """
    Instantiate the client with your login ID and transaction key from
    borgun.is.

    The ``debug`` option determines whether to use debug mode for the APIs.
    This should be ``True`` in development and staging, and should be ``False``
    in production when you want to actually process credit cards. You will need
    to pass in the appropriate login credentials depending on debug mode. The
    ``test``option determines whether to run the standard API in test mode,
    which should generally be left ``False``, even in development and staging
    environments.
    """

    # The protocol version
    VERSION = "1000"

    def __init__(self, username, password, processor, merchant_id, contract_number, debug=True, test=False):
        """
        :param str username: The username
        :param str password: The password
        :param str processor: The processor
        :param str merchant_id: The merchant id
        :param str contract_number: The contract number
        :param bool debug: Debug
        :param bool test: Test
        """
        self.processor = processor
        self.merchant_id = merchant_id
        self.contract_number = contract_number
        self.debug = debug
        self.test = test

        url = TEST_URL if test else PROD_URL

        print("The url is: ", url)
        from borgun.transport import HttpAuthenticatedV3
        transport = HttpAuthenticatedV3(username=username, password=password)
        self._client = SudsClient(url=url, transport=transport)

    def capture(self, amount, credit_card, rrn):
        """
        Accepts an amount in ISK only. Pass in a
        `CreditCard <borgun.data.CreditCard>` instance. This wil return an :class:

        :param int amount: Amount
        :param CreditCard credit_card: Credit card
        :param str rrn: RRN
        :rtype: BorgunResponse

        :raises ValueError: If rrn is missing
        """
        if type(credit_card) is not CreditCard:
            raise ValueError("credit_card must be BorgunCreditCard, got %s"
                             % type(credit_card))

        if not rrn:
            raise ValueError("missing required parameter 'rrn'")
        now = datetime.now()
        now_fmt = "%y%m%d%H%M%S"

        req = self._client.factory.create("getAuthorizationInput")

        xml = ['<?xml version="1.0" encoding="utf-8"?>']
        xml.append('<getAuthorization>')
        xml.append('<Version>%s</Version>' % BorgunClient.VERSION)
        xml.append('<Processor>%s</Processor>' % self.processor)
        xml.append('<MerchantID>%s</MerchantID>' % self.merchant_id)
        # 1 => Normal transaction
        xml.append('<TransType>%s</TransType>' % "1")
        # amount in aurar / cents
        xml.append('<TrAmount>%d</TrAmount>' % (amount * 100))
        xml.append('<TrCurrency>%s</TrCurrency>' % "352")  # 352 => ISK
        xml.append('<DateAndTime>%s</DateAndTime>' % now.strftime(now_fmt))

        xml.append('<PAN>%s</PAN>' % credit_card.card_number)
        xml.append('<ExpDate>%s%s</ExpDate>' %
                   (credit_card.exp_year, credit_card.exp_month))
        xml.append('<RRN>%s</RRN>' % rrn)
        xml.append('<CVC2>%s</CVC2>' % credit_card.cvv)
        xml.append('</getAuthorization>')

        authreqxml = "".join(xml)

        trans = self._client.service.getAuthorization(authreqxml)

        # suds has decoded already but ET wants it encoded
        trans_iso = trans.encode('iso-8859-1')
        print trans_iso
        log.debug("%s" % trans_iso)

        root = ET.fromstring(trans_iso)
        processor = root.findtext("Processor")
        merchant = root.findtext("MerchantID")
        terminal = root.findtext("TerminalID")
        pan = mask_cc(root.findtext("PAN"))
        auth_code = root.findtext("AuthCode")
        action_code = root.findtext("ActionCode")
        rrn = root.findtext("RRN")
        card_acc_name = root.findtext("CardAccName")
        card_type = root.findtext("CardType")

        return BorgunResponse(
            action_code,
            trans.encode("utf-8"),
            authorization=auth_code,
            rrn=rrn,
            processor=processor,
            merchant=merchant,
            terminal=terminal,
            pan=pan,
            account_name=card_acc_name,
            card_type=card_type,
        )

    def authorize(self, amount, credit_card):
        pass
