#-*- coding: utf-8 -*-
"""
This is your main interface to the Borgun API, where you feed in your
credentials and then interact to create transactions and so forth. You will
need to sign up for your own developer account and credentials at borgun.is_.

.. _borgun.is: https://www.borgun.is/fyrirtaeki/greidslulausnir/vefgreidslur/

"""

__version__ = "$Revision: 63990 $"
# $Source$
