
#-*- coding: utf-8 -*-
import unittest
from borgun import BorgunClient
from borgun.data import CreditCard

good_card = CreditCard("5587402000012037", "2014", "09", "310", "Dick Johnson")
gateway = BorgunClient(
    username="authdev", password="ad.900",
    processor="21", merchant_id="21",
    contract_number="9256684"
)


class TestCapture(unittest.TestCase):

    def test_authorized(self):
        response = gateway.capture(100, good_card, "asdf12345678")
        expected_action_code = "000"
        return response.action_code == expected_action_code

    def test_not_authorized(self):
        response = gateway.capture(999999, good_card, "asdf12345678")
        expected_action_code = "121"
        return response.action_code == expected_action_code
