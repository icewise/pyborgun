#-*- coding: utf-8 -*-
import unittest
from borgun.client import BorgunClient
from borgun.data import CreditCard
from mock import Mock
import xml.etree.ElementTree as ET

TEST_XML1 = """<?xml version='1.0' encoding="utf-8"?>
<getAuthorizationReply>
  <Version>1000</Version>
  <Processor>21</Processor>
  <MerchantID>21</MerchantID>
  <TerminalID>1</TerminalID>
  <TransType>1</TransType>
  <TrAmount>000000010000</TrAmount>
  <TrCurrency>352</TrCurrency>
  <DateAndTime>140404221801</DateAndTime>
  <PAN>5587402000012037</PAN>
  <RRN>asdf12345678</RRN>
  <Transaction>130</Transaction>
  <Batch>112</Batch>
  <CardAccId>9256684</CardAccId>
  <CardAccName>Test</CardAccName>
  <AuthCode>969450</AuthCode>
  <ActionCode>000</ActionCode>
  <StoreTerminal>00010001</StoreTerminal>
  <CardType>MasterCard</CardType>
</getAuthorizationReply>"""


class TestCapture(unittest.TestCase):

    def setUp(self):
        self.good_card = CreditCard("5587402000012037", "2014", "09", "310", "Dick Johnson")
        self.gateway = BorgunClient(
            username="authdev", password="ad.900",
            processor="21", merchant_id="21",
            contract_number="9256684", test=True
        )

    def test_authorized(self):
        response = self.gateway.capture(100, self.good_card, "asdf12345678")
        expected_action_code = "000"
        self.assertEqual(response.action_code, expected_action_code)

    def test_not_authorized(self):
        response = self.gateway.capture(999999, self.good_card, "asdf12345678")
        expected_action_code = "121"
        self.assertEqual(response.action_code, expected_action_code)

    def test_parsing_from_xml(self):
        self.gateway._client = Mock()
        self.gateway._client.service.getAuthorization.return_value = TEST_XML1
        response = self.gateway.capture(100, self.good_card, "asdf12345678")
        self.assertEqual(response.authorization, "969450")
        self.assertEqual(response.rrn, "asdf12345678")
        self.assertEqual(response.processor, "21")
        self.assertEqual(response.merchant, "21")
        self.assertEqual(response.terminal, "1")
        self.assertEqual(response.pan, "************2037")
        self.assertEqual(response.account_name, "Test")
        self.assertEqual(response.card_type, "MasterCard")

    def test_parsing_from_xml(self):
        self.gateway._client = Mock()
        self.gateway._client.service.getAuthorization = Mock(side_effect=self.side_effect)
        response = self.gateway.capture(100, self.good_card, "asdf12345678")

    def side_effect(self,value):
        print("Getting value:", value)
        root = ET.fromstring(value)
        version = root.findtext("Version")
        processor = root.findtext("Processor")
        merchant = root.findtext("MerchantID")
        transtype = root.findtext("TransType")
        tramount = root.findtext("TrAmount")
        trcurrency = root.findtext("TrCurrency")
        dateandtime = root.findtext("DateAndTime")
        pan = root.findtext("PAN")
        expdate = root.findtext("ExpDate")
        rrn = root.findtext("RRN")
        cvc2 = root.findtext("CVC2")
        self.assertEqual(root.tag, "getAuthorization")
        self.assertEqual(version, "1000")
        self.assertEqual(processor, "21")
        self.assertEqual(merchant, "21")
        self.assertEqual(transtype, "1")
        self.assertEqual(tramount, "10000")
        self.assertEqual(trcurrency, "352")
        #self.assertEqual(dateandtime, "140404230729")
        self.assertEqual(pan, "5587402000012037")
        self.assertEqual(expdate, "1409")
        self.assertEqual(rrn, "asdf12345678")
        self.assertEqual(cvc2, "310")

        return TEST_XML1
