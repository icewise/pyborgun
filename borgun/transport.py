#!/usr/bin/python
import httplib
import ssl
import urllib2
import socket

from suds.transport.https import HttpAuthenticated
from suds.transport.http import HttpTransport


'''
The Borgun gateway only answers SSLv3 connections and refuses SSLv23 connections.

This module fixes the SSL23_GET_SERVER_HELLO bug by forcing SSLv3.
'''


class HTTPSConnectionV3(httplib.HTTPSConnection):

    def __init__(self, *args, **kwargs):
        httplib.HTTPSConnection.__init__(self, *args, **kwargs)

    def connect(self):
        sock = socket.create_connection((self.host, self.port), self.timeout)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        try:
            self.sock = ssl.wrap_socket(
                sock, self.key_file, self.cert_file, ssl_version=ssl.PROTOCOL_SSLv3)
        except ssl.SSLError, e:
            print("Trying SSLv3.")
            self.sock = ssl.wrap_socket(
                sock, self.key_file, self.cert_file, ssl_version=ssl.PROTOCOL_SSLv23)


class HTTPSHandlerV3(urllib2.HTTPSHandler):

    def https_open(self, req):
        """
        :rtype: urllib2.addinfourl
        """
        return self.do_open(HTTPSConnectionV3, req)


class HttpAuthenticatedV3(HttpAuthenticated):

    def u2handlers(self):
        """
        :rtype: list[urllib2.Handler]
        """
        handlers = HttpTransport.u2handlers(self)
        handlers.append(urllib2.HTTPBasicAuthHandler(self.pm))
        handlers.append(HTTPSHandlerV3())
        return handlers
